from pytest import mark

from . import jinjaproc


@mark.parametrize('template_string,vars,expected', [
    (
        "{\"hello\": \"{{ name.title() }}\"}",
        {"name": "john"},
        "{\"hello\": \"John\"}"
    )])
def test_render_string(template_string, vars, expected):
    result = jinjaproc.render_string(template_string=template_string, vars=vars)
    assert result == expected