# jinja template processing module
import json

from jinja2 import Environment

extensions = [
    'jinja2.ext.loopcontrols', 
    'jinja2.ext.with_', 
    'jinja2.ext.do', 
    'jinja2.ext.debug'
]

jinja_environment = Environment(extensions=extensions)
globals = {}


def render_string(template_string, *, vars):
    return jinja_environment.from_string(template_string, globals=globals).render(vars)
