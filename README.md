# jaas

Jinja as a Service

A web API to manage and render Jinja2 templates built using AWS services.

## Purpose 

The purpose of this project is to practice using various AWS services by exposing a simple text transformation service.

## Jinja 

[Jinja](https://jinja.palletsprojects.com/en/2.11.x/templates/) is a powerful templating language which can render text containing variables or expressions to generate a rich/dynamic text.

## Requirements

* An AWS account
* [Poetry](https://python-poetry.org/)
* Python 3.7

### AWS Features

* Lambda
* CloudFormation
* API Gateway
* IAM
