MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
INSTALL = poetry install
RUN = poetry run
POETRY_VERSION = $(poetry --version)


help:  ## Prints this help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'


poetry-version:  ## Prints the current verion of Poetry
	@poetry --version

install: poetry-version  ## Install/upgrade project and development requirements in virtual environment.
	$(INSTALL)

test: ## Execute BPI test suite
	$(RUN) pytest lambdas/. --junitxml=./test_report.xml
